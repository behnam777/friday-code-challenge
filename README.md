#### FRIDAY Code Challenge
This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

Below you will find some information on how to perform common tasks.<br>

## Info

This app is created using create-react-app cli along with redux and promise-middleware

### Project Structure

```
- public: contains static files, serviceWorker and manifest

- src
    -- components: small components of project
    -- containers: Pages of app
    -- store: configs for redux and shared store
    index.js: initiate react app and router
```

## Running in Dev mode

In the project directory run:

`npm install` or `yarn install`

which will fetch development requirements

then run:

`npm run start` or `yarn start`

and will run project on localhost:3000

## Building and Deployment

In the project directory run:

`npm install` or `yarn install`

which will fetch development requirements

then run:

`npm run build` or `yarn build`

which will build project into build/ folder

You could run project by running:

`serve -s build/`


## Testing
For the tests to execute, run:

`npm run test` or `yarn test`

Launches the test runner in the interactive watch mode.<br>

