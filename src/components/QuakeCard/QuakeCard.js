import React from 'react';
import PropTypes from 'prop-types';
import {isEmpty} from 'lodash';
import moment from 'moment';
import './QuakeCard.css';

export default class QaukeCard extends React.Component {

  static propTypes = {
    data: PropTypes.object,
    title: PropTypes.string
  };

  render() {
    if (!isEmpty(this.props.data)) {
      let {properties} = this.props.data;
      return (
        <div className="container">
          <div className="title">
            {this.props.title}
          </div>
          <div className="info">
            <div>
              <span className="label">Place</span>
              <span className="value">{properties.place}</span>
            </div>
            <div>
              <span className="label">Magnitude</span>
              <span className="value">{properties.mag}</span>
            </div>
            <div>
              <span className="label">Date/Time</span>
              <span className="value">{moment(properties.time,'x').format('DD/MM/YYYY HH:mm:ss')}</span>
            </div>
          </div>
        </div>
      );
    } else {
      return null;
    }
  }
}