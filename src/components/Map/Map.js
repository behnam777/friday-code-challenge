import React from "react";
import PropTypes from 'prop-types';

let marker, map, markers = [];

export default class Map extends React.Component {

  static propTypes = {
    center: PropTypes.object,
    markers: PropTypes.array,
    lastUpdateTime: PropTypes.number,
    zoom: PropTypes.number
  };

  state = {
    center: this.props.center || {lat: 0, lng: 0},
    zoom: this.props.zoom || 3,
    exactValue: null
  };

  /**
   * on component mount, initiate map and draw markers on it
   */
  componentDidMount() {
    this.initMap();
    this.drawMarkers(this.props.markers);
  }

  /**
   * On changing the lastUpdateTime, reload markers
   * @param nextProps
   */
  componentWillReceiveProps(nextProps) {
    if (nextProps.markers.length > 0 && nextProps.lastUpdateTime > this.props.lastUpdateTime) {
      //clear old markers
      markers = [];
      //draw new markers
      this.drawMarkers(nextProps.markers);
    }
  }

  /**
   * initiate map
   */
  initMap = () => {
    if (window.google === undefined) {
      setTimeout(this.initMap, 1000);
    } else {
      map = new window.google.maps.Map(document.getElementById('map'), {
        center: this.state.center,
        zoom: this.state.zoom,
        clickableIcons: false
      });
    }
  };

  /**
   * call addMarker method for every marker in list
   * @param markers
   */
  drawMarkers = (markers) => {
    if (markers && markers.length) {
      markers.forEach(marker => {
        this.addMarker({...marker})
      })
    }
  };

  /**
   * add marker to markers list.
   * set marker icon and infoWindow of marker
   * @param data
   */
  addMarker = data => {
    let {lat, lng, info} = data;
    marker = new window.google.maps.Marker({
      position: {lat, lng},
      map: map,
      icon: '/images/bullseye.png'
    });

    //initiate infoWindow
    let infoWindow = new window.google.maps.InfoWindow({
      content: `
      <div>
        <div>
          <span>
          <b>Location:</b>
          ${info.place}
          </span>
        </div>
      </div>
      `
    });

    //add click event listener for marker
    marker.addListener('click', function(){
      infoWindow.open(map, this);
    });

    //add to list of markers
    markers.push(marker);
  };

  render() {
    return (
      <div style={{height: 400}} id="map"></div>
    );
  }
};