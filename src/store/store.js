import {createStore, applyMiddleware, compose} from "redux";
import promiseMiddleware from "redux-promise-middleware";
import reducers from "./reducers";

//make devtools available
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

//apply promise middleware
const middleware = composeEnhancers(
  applyMiddleware(
    promiseMiddleware(),
  )
);

//initial state
const initialState = {
};

//create store
const store = createStore(
  reducers,
  initialState,
  middleware
);

export default store;