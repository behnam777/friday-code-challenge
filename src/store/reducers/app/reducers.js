import {
  GET_DATA
} from "./";

const initial = {
  //indicate that rest is loading
  isLoading: true,
  metadata: {},
  features: [],
  bbox: []
};

const data = (state = initial, action) => {
  switch (action.type) {
    case `${GET_DATA}_FULFILLED`:
      return {...state, ...action.payload, isLoading: false};

    default:
      return state
  }
};

export default data;