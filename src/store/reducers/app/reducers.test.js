import React from 'react';
import * as actions from './'
import reducer from './reducers';


describe('App reducer', () => {
  it('initial state', () => {
    expect(reducer(undefined, {})).toEqual(
      {
        isLoading: true,
        metadata: {},
        features: [],
        bbox: []
      }
    )
  });

  it('should handle get geoData', () => {
    const payload = {
      metadata:{foo:'bar'},
      features:[{bar:'foo'}],
      bbox:[10,15]
    };
    expect(
      reducer([], {
        type: `${actions.GET_DATA}_FULFILLED`,
        payload
      })
    ).toEqual(
      {
        isLoading: false,
        ...payload
      }
    )
  });

});