import Rest from '../../../utils/rest';

export const
  GET_DATA = 'APP/GET/DATA';

export const getGeoData=()=>{
  return {
    type:GET_DATA,
    payload: Rest.get(`https://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/all_day.geojsonp`)
  }
};


