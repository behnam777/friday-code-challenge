import React from 'react';
import PropTypes from 'prop-types';
import './App.css';
import Map from '../../components/Map';
import QuakeCard from '../../components/QuakeCard'
import {get} from 'lodash';

class App extends React.Component {
  static propTypes = {
    isLoading: PropTypes.bool,
    getGeoData: PropTypes.func,
    updateData: PropTypes.func,
    features: PropTypes.array,
    metadata: PropTypes.object,
    refreshInterval: PropTypes.number,
    tick: PropTypes.number
  };

  static defaultProps = {
    refreshInterval: 30,
    tick: 100
  };

  state = {
    markers: [],
    lastUpdateTime: 0,
    progress: 0,
    min: {},
    max: {}
  };

  intervalId = -1;

  /**
   * On component mount, get geoData
   */
  componentDidMount() {
    this.props.getGeoData();
  }

  /**
   * on metadata changes, make new markers and set min and max
   * @param nextProps
   */
  componentWillReceiveProps(nextProps) {
    if (get(nextProps, 'metadata.generated', 0) > get(this.props, 'metadata.generated', 1) && nextProps.features.length) {
      this.populateMarkers(nextProps.features, nextProps.metadata.generated);
      this.getMinAndMax(nextProps.features);
      this.startTicker();
    }
  };

  /**
   * on component un-mount clear time interval if exists
   */
  componentWillUnmount() {
    if (this.intervalId > -1) {
      clearInterval(this.intervalId);
    }
  };

  /**
   * start ticking and move progress along
   * if progress reaches 100, call getGeoData again
   */
  startTicker = () => {
    if (this.intervalId === -1) {
      this.intervalId = setInterval(() => {
        if (this.state.progress < 100) {
          this.setState({progress: this.state.progress + (1000 / (this.props.tick * this.props.refreshInterval))})
        } else {
          this.setState({progress: 0});
          this.props.getGeoData()
        }
      }, this.props.tick);
    }
  };

  /**
   * make marker object
   * @param list
   * @param lastUpdateTime
   */
  populateMarkers = (list, lastUpdateTime) => {
    let markers = [];
    if (list && list.length) {
      list.forEach(feature => {
        markers.push({
          lng: feature.geometry.coordinates[0],
          lat: feature.geometry.coordinates[1],
          info: feature.properties
        })
      })
    }
    this.setState({markers, lastUpdateTime});
  };

  /**
   * get min and max magnitude in list
   * @param list
   */
  getMinAndMax = (list) => {
    if (list && list.length) {
      //sort list by magnitude
      list.sort((a, b) => {
        if (a.properties.mag >= b.properties.mag) {
          return 1
        }
        return -1
      });
      //get first and last item in sorted list
      this.setState({min: list[0], max: list[list.length - 1]});
    } else {
      this.setState({min: {}, max: {}});
    }
  };

  renderMap = () => {
    if (this.props.isLoading) {
      return (
        <div>
          <div>
            <span>
              Loading data...
            </span>
          </div>
          <span className="spinner"></span>
        </div>
      );
    } else {
      return (
        <div>
          <div className="progress-bar-container"
               title={`refreshing data in ${((1 - (this.state.progress / 100)) * this.props.refreshInterval).toFixed(1)} seconds`}>
            <span className="progress-bar" style={{width: `${this.state.progress}%`}}/>
          </div>
          <Map markers={this.state.markers} lastUpdateTime={this.state.lastUpdateTime}/>
        </div>
      );
    }
  };

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <h1 className="App-title">Welcome to earthquake map</h1>
        </header>
        {this.renderMap()}
        <div>
          <QuakeCard data={this.state.min} title="MIN"/>
          <QuakeCard data={this.state.max} title="MAX"/>
        </div>
      </div>
    );
  }
}
export default App;