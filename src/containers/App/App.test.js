import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import {Router,Route} from 'react-router';
import createBrowserHistory from "history/createBrowserHistory"

it('renders without crashing', () => {
  const div = document.createElement('div');
  const history = createBrowserHistory();

  ReactDOM.render(()=>(
    <Router history={history}>
      <Route path='/' component={App}/>
    </Router>
  ), div);
});
