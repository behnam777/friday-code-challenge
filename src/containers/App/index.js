import App from './App';
import {connect} from 'react-redux';
import {getGeoData} from '../../store/reducers/app';

const mapStateToProps = (state) => ({
  isLoading:state.app.isLoading,
  features:state.app.features,
  metadata:state.app.metadata
});

const mapDispatchToProps = (dispatch) => ({
  getGeoData: ()=>dispatch(getGeoData())
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
