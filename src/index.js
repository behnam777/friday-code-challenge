import createBrowserHistory from "history/createBrowserHistory"
import store from "./store";
import App from "./containers/App";
import React from "react";
import ReactDOM from "react-dom";
import {Provider} from "react-redux";
import {Router, Route, Switch} from "react-router-dom";

//create history
const history = createBrowserHistory();


ReactDOM.render(
  <Provider store={store}>
    <Router history={history}>
      <Switch>
        <Route exact path="/" component={App}/>
      </Switch>
    </Router>
  </Provider>,
  document.getElementById("root")
);
