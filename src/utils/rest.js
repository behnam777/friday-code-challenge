/**
 * Check json status and return json or throw error
 * @param json
 * @returns {*}
 */
const checkStatus = (json) => {
  if (!json || !json.metadata || json.metadata.status !== 200) {
    throw new Error('Error in fetching data');
  } else {
    return json;
  }
};

/**
 * parse rest result and return text
 * @param res
 * @returns {{}}
 */
const parseJSON = (res) => {
  if (res.headers.get("Content-Type")) {
    return res.text();
  }
  else {
    return {};
  }
};

/**
 * parse jsonP text and extract json
 * @param res
 */
const parseJsonP=(res)=>{
  res = res.replace(/\r?\n|\r/g,'');
  const match = res.match(/eqfeed_callback\((.*)\);/);
  if (! match) throw new Error('invalid JSONP response');
  return JSON.parse(match[1]);
};

/**
 * display error in console
 * @param e
 */
const showError = (e) => {
  //on error, show general errors messages
  if (e && e.status) {
    if (e.status === 404) {
      console.log('Api not found!')
    }
  }
  throw e;
};

/**
 * using fetch to get from uir
 * @param uri
 */
const get = (uri) => fetch(uri)
  .then(parseJSON)
  .then(parseJsonP)
  .then(checkStatus)
  .catch(showError);


export default {get, checkStatus, parseJSON};